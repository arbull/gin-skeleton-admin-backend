module goskeleton

go 1.15

require (
	github.com/casbin/casbin/v2 v2.24.0
	github.com/casbin/gorm-adapter/v3 v3.2.0
	github.com/dchest/captcha v0.0.0-20170622155422-6a29415a8364
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gin-contrib/pprof v1.3.0
	github.com/gin-gonic/gin v1.7.1
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/gorilla/websocket v1.4.2
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/mitchellh/mapstructure v1.3.1 // indirect
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/qifengzhang007/goCurl v1.3.2
	github.com/qifengzhang007/sql_res_to_tree v1.0.9
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v1.1.0
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/viper v1.7.0
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	go.uber.org/zap v1.16.0
	golang.org/x/sys v0.0.0-20200828194041-157a740278f4 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/ini.v1 v1.56.0 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
	gorm.io/driver/mysql v1.0.5
	gorm.io/driver/postgres v1.0.4
	gorm.io/driver/sqlserver v1.0.5
	gorm.io/gorm v1.21.8
	gorm.io/plugin/dbresolver v1.1.1-0.20210422053029-0a33f92e93ec
)
